from fastapi import FastAPI, Body, Path, Query, Request, HTTPException, Depends
from fastapi.responses import HTMLResponse, JSONResponse
from fastapi.security.http import HTTPAuthorizationCredentials
from pydantic import BaseModel, Field
from typing import Optional, List, Coroutine
from jwt_manager import create_token, validate_token
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from fastapi.encoders import jsonable_encoder
from routers.categorias import categorias_router
from routers.libro import libro_router
from routers.user import user_router
from middlewares.error_handler import ErrorHandler
from config.database import Base, engine, Session
from fastapi.middleware.cors import CORSMiddleware;

# eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGdtYWlsLmNvbSIsInBhc3N3b3JkIjoiYWRtaW4ifQ.ERWUnQJ6Fi5mlIC_t9ejAk4PjOuKMr7Jv02CltW-5N0

app = FastAPI()
app.title="Biblioteca Virtual Uro por favor llega mas temprano"

app.add_middleware(ErrorHandler)
app.include_router(categorias_router)
app.include_router(libro_router)
app.include_router(user_router)

Base.metadata.create_all(bind=engine)

app.add_middleware(
    CORSMiddleware,
    allow_origins = ["*"],
    allow_credentials = True,
    allow_methods = ["*"],
    allow_headers = ["*"]
)