import os;
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

sqlite_file_name = "../database.sqlite"

# Estamos leyendo el directorio actual que es database.py
base_dir = os.path.dirname(os.path.realpath(__file__))

# Creamos la url de la base de datos uniendo las 2 variables anteriores
databases_url = f"sqlite:///{os.path.join(base_dir, sqlite_file_name)}"

# Creamos el motor de la base de datos con la url
engine = create_engine(databases_url, echo=True)

# Creamos la sesión de la BD con el motor 
Session = sessionmaker(bind=engine)

# Creamos la base de datos declarativa
Base = declarative_base()

# Crea todas las tablas en la base de datos
Base.metadata.create_all(bind=engine)
