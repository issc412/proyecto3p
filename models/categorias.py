from config.database import Base
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship


class Categorias(Base):
    __tablename__ = "categorias"
    
    id_categoria = Column(Integer, primary_key=True, index=True)  # Asegúrate de que esta línea esté presente
    nombre = Column(String, unique=True)
    libros = relationship("Libro", back_populates="categoria")  # Relación inversa a libros