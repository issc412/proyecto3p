from config.database import Base
from sqlalchemy import Column, Integer, String, Float
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from models.categorias import Categorias

class Libro(Base):
    __tablename__ = "libros"
    
    id_libro = Column(Integer, primary_key=True, index=True)
    titulo = Column(String, index=True)
    autor = Column(String)
    año = Column(Integer)
    nombre_categoria = Column(String, ForeignKey('categorias.nombre'))  # Referencia al ID de la categoría
    categoria = relationship("Categorias", back_populates="libros")  # Establece la relación con el modelo Categorias
    numPaginas = Column(Integer)