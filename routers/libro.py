from fastapi import FastAPI, Body, Path, Query, Request, HTTPException, Depends
from fastapi.responses import HTMLResponse, JSONResponse
from fastapi.security.http import HTTPAuthorizationCredentials
from pydantic import BaseModel, Field
from typing import Optional, List, Coroutine
from jwt_manager import create_token, validate_token
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from fastapi.encoders import jsonable_encoder
from fastapi import APIRouter
from middlewares.jwt_bearer import JWTBearer
from config.database import Session
from models.categorias import Categorias as CategoriaModel
from models.libro import Libro as LibroModel
from models.libro import Libro


libro_router = APIRouter()

# Clase Principal: LIBRO
class Libro(BaseModel):
    id_libro: Optional[int] = None 
    titulo: str = Field(max_length=150) 
    autor: str = Field(min_length=1) 
    año: int = Field(le=2024) 
    nombre_categoria: str = Field(min_length=3)
    numPaginas: int = Field(..., ge=0)
    
    class Config:
        schema_extra = {
            "example":{
                "id_libro": 1,
                "titulo": "Titulo de libro",
                "autor": "Autor del libro",
                "año": "Año de publicación",
                "nombre_categoria": {"nombre": "nombre de categoría"},
                "numPaginas": "Número de páginas"
            }
        }

libros = []

id_counter_categoria = 1

categorias = []

id_counter_libro = 1 

# Metodo para visualizar la biblioteca (lista de libros) #############################################################################################################################
@libro_router.get('/libros/', tags=['Libros'], response_model= List[Libro], status_code=200)
def get_libros() -> List[Libro]:
    db = Session()
    libros = db.query(LibroModel).all()
    return libros

# Metodo para buscar un libro por id ###################################################################################################################################

@libro_router.get('/libros/{id_libro}',tags=['Libros'], status_code=200)
def get_libros_by_id(id_libro: int):
    db = Session()
    result = db.query(LibroModel).filter(LibroModel.id_libro == id_libro).first()
    if result is not None:
        return result
    else:
        raise HTTPException(status_code=404, detail="Libro no encontrado")
    
# Metodo para crear un libro ###################################################################################################################################
@libro_router.post('/libros/', tags=['Libros'], status_code=200, dependencies=[Depends(JWTBearer())])
def create_libros(titulo: str = Body(...), autor: str = Body(...), año: int = Body(...), numPaginas: int = Body(...), nombre_categoria: str = Body(...)):
    db = Session()

    # Verificar si la categoría existe
    categoria_encontrada = db.query(CategoriaModel).filter(CategoriaModel.nombre == nombre_categoria).first()
    if not categoria_encontrada:
        raise HTTPException(status_code=400, detail="Categoría no válida. Debe crear esta categoría antes de agregar el libro.")

    # Crear el nuevo libro con el id_categoria de la categoría encontrada
    nuevo_libro = LibroModel(titulo=titulo, autor=autor, año=año, nombre_categoria=categoria_encontrada.nombre, numPaginas=numPaginas)

    # Agregar el nuevo libro a la lista de libros
    db.add(nuevo_libro)
    db.commit()
    return {"message": "Creaste un libro exitosamente.", "Su ID es: ": nuevo_libro.id_libro}

# Metodo para buscar por categoría ###################################################################################################################################
@libro_router.get('/libros/categoria/{categoria}', tags=['Libros'], status_code=200)
def get_libros_by_categoria(categoria: str):
    db = Session()
    libros_encontrados = db.query(LibroModel).filter(LibroModel.categoria.has(nombre=categoria)).all()
    if not libros_encontrados:
        raise HTTPException(status_code=404, detail="No se encontraron libros en esta categoría")
    return libros_encontrados

# Metodo para eliminar un libro por id ###################################################################################################################################
@libro_router.delete('/libros/{id_libro}', tags=['Libros'], status_code=200, dependencies=[Depends(JWTBearer())])
def delete_libros(id_libro: int):
    db = Session()
    libro = db.query(LibroModel).filter(LibroModel.id_libro == id_libro).first()
    if not libro:
        raise HTTPException(status_code=404, detail="El Id del libro indicado no existe")
    db.delete(libro)
    db.commit()
    return JSONResponse(status_code = 200, content = {"message": "Se ha eliminado el libro con éxito."})

# Metodo para editar un libro ######################################################################################################################################
@libro_router.put('/libros/{id_libro}', tags=['Libros'], status_code=200, dependencies=[Depends(JWTBearer())])
def update_libro(id_libro: int, titulo: str = Body(...), autor: str = Body(...), año: int = Body(...), numPaginas: int = Body(...), nombre_categoria: str = Body(...)):
    db = Session()
    libro_encontrado = db.query(LibroModel).filter(LibroModel.id_libro == id_libro).first()
    if not libro_encontrado:
        raise HTTPException(status_code=404, detail="Libro no encontrado")

    # Verificar si la categoría existe y buscar el objeto de categoría
    categoria = db.query(CategoriaModel).filter(CategoriaModel.nombre == nombre_categoria).first()
    if not categoria:
        raise HTTPException(status_code=400, detail="La categoría no existe. Debe crear esta categoría antes de agregar el libro.")

    # Actualizar el libro
    libro_encontrado.titulo = titulo
    libro_encontrado.autor = autor
    libro_encontrado.año = año
    libro_encontrado.numPaginas = numPaginas

    # Asignar la nueva categoría
    libro_encontrado.categoria = categoria
    
    db.commit()
    return {"message": "Libro actualizado correctamente"}