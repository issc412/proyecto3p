from fastapi import FastAPI, Body, Path, Query, Request, HTTPException, Depends
from fastapi.responses import HTMLResponse, JSONResponse
from fastapi.security.http import HTTPAuthorizationCredentials
from pydantic import BaseModel, Field
from typing import Optional, List, Coroutine
from jwt_manager import create_token, validate_token
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from fastapi.encoders import jsonable_encoder
from fastapi import APIRouter
from middlewares.jwt_bearer import JWTBearer
from config.database import Session
from models.libro import Libro as LibroModel
from models.categorias import Categorias as CategoriasModel


categorias_router = APIRouter()

# Clase Secundaria: CATEGORIA
class Categorias(BaseModel):
    id_categoria: Optional[int] = None
    nombre: str = Field(min_length=3)
        
    class Config: 
        schema_extra = {
            "example":{
                "id_categoria": 0,
                "nombre": "Nombre de la categoria"
            }
        }


libros = []

id_counter_categoria = 1

categorias = []

id_counter_libro = 1 


@categorias_router.post('/categorias/', response_model=Categorias, tags=['Categorías'], status_code=200, dependencies=[Depends(JWTBearer())])
def create_categoria(categoria: Categorias):
    db = Session()

    # Verificar si la nueva categoría ya existe
    categoria_existente = db.query(CategoriasModel).filter(CategoriasModel.nombre == categoria.nombre).first()
    if categoria_existente:
        raise HTTPException(status_code=400, detail="La categoría ya existe")

    # Crear una nueva instancia de CategoriasModel y agregarla a la base de datos
    nueva_categoria = CategoriasModel(nombre=categoria.nombre)
    db.add(nueva_categoria)
    db.commit()
    return categoria

# Endpoint para eliminar una categoria #############################################################################################################################
@categorias_router.delete('/categorias/{nombre}', tags=['Categorías'], status_code=200, dependencies=[Depends(JWTBearer())])
def eliminar_categoria(nombre: str):
    db = Session()

    # Verificar si algún libro pertenece a esta categoría
    libros_con_categoria = db.query(LibroModel).filter(LibroModel.categoria.has(nombre=nombre)).all()    
    if libros_con_categoria:
        raise HTTPException(status_code=400, detail="No se puede eliminar una categoría que tiene libros asociados")

    # Encontrar la categoría en la lista de categorías
    categoria = db.query(CategoriasModel).filter(CategoriasModel.nombre == nombre).first()
    if not categoria:
        raise HTTPException(status_code=404, detail="Categoria no encontrada")

    # Eliminar la categoría
    db.delete(categoria)
    db.commit()
    return {"message": "Categoria eliminada correctamente"}


# Metodo para editar una categoria ##########################################################################################################################################
@categorias_router.put('/categorias/{nombre}', tags=['Categorías'], status_code=200, dependencies=[Depends(JWTBearer())])
def update_categoria(nombre: str, nueva_categoria: Categorias):
    db = Session()

    # Encontrar la categoría en la lista de categorías
    categoria = db.query(CategoriasModel).filter(CategoriasModel.nombre == nombre).first()
    if not categoria:
        raise HTTPException(status_code=404, detail="Categoria no encontrada")

    # Verificar si la nueva categoría ya existe
    categoria_existente = db.query(CategoriasModel).filter(CategoriasModel.nombre == nueva_categoria.nombre).first()
    if categoria_existente:
        raise HTTPException(status_code=400, detail="La categoría ya existe")

    # Actualizar la categoría
    categoria.nombre = nueva_categoria.nombre
    db.commit()

    # Actualizar el nombre de la categoría en los libros que la tienen asignada
    libros = db.query(LibroModel).filter(LibroModel.nombre_categoria == nombre).all()
    for libro in libros:
        libro.nombre_categoria = nueva_categoria.nombre

    db.commit()

    return {"message": "Categoría y libros actualizados correctamente"}


# Metodo para obtener todas las categorias ##################################################################################################################################
@categorias_router.get('/categorias', tags=['Categorías'], response_model=List[Categorias], status_code=200)
def get_categorias() -> List[Categorias]:
    db = Session()
    categorias = db.query(CategoriasModel).all()
    return categorias